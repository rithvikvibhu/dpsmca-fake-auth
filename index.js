const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');

var port = process.env.PORT || 3000;
app.set('port', port);

var admin = require("firebase-admin");
if (process.env.serviceAccountKey) {
  var serviceAccount = JSON.parse(process.env.serviceAccountKey);
} else {
  var serviceAccount = require("./serviceAccountKey.json");
}
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://dpsemca-1f00a.firebaseio.com"
});

app.use(express.static('public'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.post('/login', function (req, res) {
  console.log(req.body);
  admin.auth().createCustomToken(req.body.admid).then(function(customToken) {
    res.cookie('token', customToken, {maxAge: 1000 * 60 * 15});
    res.redirect('https://auth.dpscanteen.ml/?token='+customToken);
  })
  .catch(function(error) {
    console.log("Error creating custom token:", error);
    res.json({error: error});
  });
});

app.listen(port, function () {
  console.log('Example app listening on port', port);
});
